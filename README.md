# task log

## 手動追加

```
最低限この3つ:
  state: "done" // 完了にしないと集計対象にならない
  estimated: 適当見積もり
  actual: 実際にかかった時間

{
  "id": "task-satou-1",
  "name": "タスク satou-1",
  "worker": "satou"
  "state": "done",
  "estimated": 10,
  "actual": 15
}
```
