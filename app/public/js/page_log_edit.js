class TaskRow {
  static render(state, t) {
    let cls = "";
    if (t.id === state.selectedTaskId) {
      cls += " selected";
    }

    let time = __p.getTimeOfTask(t.id);

    let selected = state.workTasks.some(wt => wt.id === t.id && 0 < wt.time);
    let checkboxAttrs = {};
    if (selected) {
      checkboxAttrs.checked = "checked";
    }

    if (!selected) {
      cls += " non_target_task"
    }

    return TreeBuilder.build(h =>
      h("tr", {
            onclick: (ev)=>{ __p.onclick_taskRow(ev, t.id); }
          , "class": cls
          }
        , h("td", {}
          , h("input", Object.assign(
                {
                  type: "checkbox"
                , onclick: (ev)=>{ __p.onchange_taskCheckbox(ev, t.id); }
                }
              , checkboxAttrs
              )
            )
          )
        , h("td", {}, t.id)
        , h("td", {}, t.name)
        , h("td", {}, t.worker)
        , h("td", {}, t.state)
        , h("td", {}, time)
      )
    );
  }
}

class View {
  static render(state) {
    return TreeBuilder.build(h =>
      h("div", {}

      , h("h1", {}, `${__p.getWorker()} ${__p.getDate()}`)
        
      , h("table", {}
        , h("tr", {}
          , h("th", {}, "")
          , h("th", {}, "id")
          , h("th", {}, "name")
          , h("th", {}, "worker")
          , h("th", {}, "state")
          , h("th", {}, "time (h)")
          )
        , state.tasks
            .filter(t => t.worker === __p.getWorker())
            .filter(t => t.state !== "done")
            .map(t => TaskRow.render(state, t))
        )

      , h("div", {}
        , MyRadioGroup.render(
            "radiogrp_time"
          , [
              { value: 0.5, text: "0.5" }
            , { value: 1, text: "1" }
            , { value: 1.5, text: "1.5" }
            , { value: 2, text: "2" }
            , { value: 2.5, text: "2.5" }
            , { value: 3, text: "3" }
            , { value: 3.5, text: "3.5" }
            , { value: 4, text: "4" }
            , { value: 4.5, text: "4.5" }
            , { value: 5, text: "5" }
            , { value: 5.5, text: "5.5" }
            , { value: 6, text: "6" }
            , { value: 6.5, text: "6.5" }
            , { value: 7, text: "7" }
            , { value: 7.5, text: "7.5" }
            , { value: 8, text: "8" }
            ]
          , {
              selected: __p.getTimeOfTask(state.selectedTaskId)
            , onchange: (ev)=>{
                __p.onchange_time(ev);
              }
            }
          )
        )

      , h("div", {}
        , "合計: "
        , h("span", { id: "sum" })
        )

      , h("hr")

      , "memo"
      , h("br")
      , h("textarea", { id: "memo"
          , style: {
              width: "80%"
            , height: "10rem"
            }
          , onchange: (ev)=>{ __p.onchange_memo(ev); }
          }
        , __p.getMemo()
        )

      , h("hr")
      , h("button", { onclick: ()=>{ __p.onsubmit(); } }, "更新")

      , h("hr")
      , h("textarea", {
            id: "edit_box"
          , oninput: ()=>{ __g.debounce(()=>{ __p.oninput_editBox(); }, 500); }
          , style: {
              width: "100%"
            , height: "80%"
            }
          }
        , JSON.stringify(state.workTasks, null, "    ")
        )

      )
    );
  }
}

class Page {
  constructor(){
    this.state = {
      tasks: [],
      logs: [],
      selectedWorker: "foo",
      selectedTaskId: "task-1"
    };
  }

  getTitle(){
    return "log edit";
  }

  preprocState(result) {
    Object.assign(this.state, result);

    const date = __p.getDate();
    const worker = __p.getWorker();

    // worker/date で絞り込んだ task ids
    const wTaskIds = this.state.tasks
          .filter(t => t.worker === worker)
          .map(w => w.id);

    // worker/date で絞り込んだ tasks
    this.state.workTasks =
      this.state.logs
      .find(log => log.date === date)
      .tasks
      .filter(t => wTaskIds.includes(t.id));
  }

  getWorker() {
    const m = location.href.match(/\/workers\/(.+?)\//);
    return m[1];
  }

  getDate() {
    const m = location.href.match(/\/logs\/(.+?)\//);
    return m[1];
  }

  getMemo() {
    const log = this.state.logs.find(log => log.date === __p.getDate());
    return log.memo;
  }

  init(){
    puts("init");
    const worker = this.getWorker();
    const date = this.getDate();

    __g.api_v2(
      "get",
      `/api/workers/${worker}/logs/${date}/edit`, {},
      (result)=>{
        __g.unguard();
        puts(result);
        this.preprocState(result);

        this.render();
        this.oninput_editBox();
      },
      (errors)=>{
        __g.unguard();
        __g.printApiErrors(errors);
        alert("Check console.");
      }
    );
  }

  render(){
    $("#tree_builder_container")
      .empty()
      .append(View.render(this.state));
    __g.refreshInputStyles();
    this.refreshSum();
  }

  onsubmit() {
    const worker = this.getWorker();
    const date = this.getDate();

    const logTasks = this.state.workTasks;

    const log = this.state.logs.find(log => log.date === __p.getDate());
    log.tasks = this.state.workTasks;

    __g.api_v2(
      "put",
      `/api/workers/${worker}/logs/${date}`,
      { log },
      (result)=>{
        __g.unguard();
        puts(result);

        // location.reload();
        location.href = `/workers/${worker}/logs`;
      },
      (errors)=>{
        __g.unguard();
        __g.printApiErrors(errors);
        alert("Check console.");
      }
    );
  }

  setTaskId(tid) {
    this.state.selectedTaskId = tid;
    this.render();
  }

  getTimeOfTask(tid) {
    const wt = this.state.workTasks.find(_t => _t.id == tid)

    if (wt) {
      return wt.time;
    } else {
      return null;
    }
  }

  refreshSum() {
    const sum = __g.sum(
      this.state.workTasks
        .map(lt => lt.time)
        .filter(time => 0 < time)
    );
    $("#sum").text(sum);
  }

  oninput_editBox() {
    try {
      const json = $("#edit_box").val();
      const lts = JSON.parse(json);
      this.state.workTasks = lts;
      this.refreshSum();
    } catch (e) {
      puts("TODO 224", e);
    }
  }

  onclick_taskRow(ev, tid) {
    this.setTaskId(tid);
  }

  onchange_time(ev) {
    const time = parseFloat(MyRadioGroup.getValue(ev));
    const wt = this.state.workTasks.find(_t => _t.id === this.state.selectedTaskId);
    if (wt == null) {
      return;
    }

    wt.time = time;
    this.render();
  }

  onchange_taskCheckbox(ev, tid) {
    const checked = ev.target.checked;

    if (checked) {
      // false => true
      // 選択されたタスクを追加

      const ts = this.state.workTasks.filter(t => t.id !== tid);

      ts.push(
        {
          id: tid,
          time: 0.5
        }
      );

      this.state.workTasks = ts;
    } else {
      // true => false
      // 選択されたタスクを除去
      this.state.workTasks = this.state.workTasks.filter(wt => wt.id !== tid);
    }

    this.render();
  }

  onchange_memo(ev) {
    const log = this.state.logs.find(log => log.date === __p.getDate());
    log.memo = ev.target.value;
  }

}

__g.ready(new Page());
