function round(x, digit) {
  if (isNaN(x)) {
    return NaN;
  } else if (x == null) {
    return null;
  }

  const p = Math.pow(10, digit);

  return Math.round(x * p) / p;
};

const fmtDate = (date) => {
  if (date.endsWith("-01")) {
    return date;
  } else {
    return date.substring(8);
  }
}

function fmtDate2(date) {
  let s = "";
  s += date.getFullYear();
  s += "-";
  s += __g.pad2(date.getMonth() + 1);
  s += "-";
  s += __g.pad2(date.getDate());
  return s;
}

function uniq(xs) {
  const map = {};
  xs.forEach(x => map[x] = null);
  return Object.keys(map);
}

function makeDate(d0, i) {
  const time = d0.getTime() + (60 * 60 * 24 * 1000 * i);
  return new Date(time);
}

let __woyMap = {};
function initWoyMap(dates) {
  __woyMap = {};

  const years =
    uniq(dates.map(d => new Date(Date.parse(d)).getFullYear()))
    .map(y => _parseInt(y));

  years.forEach(year => {
    const d0 = new Date(`${year}-01-01T12:00:00`);
    const limit = new Date(`${year + 1}-01-01T00:00:00`);
    let weekIdxOfYear = 0;

    for (let i = 0; i < 365; i++) {
      const d = makeDate(d0, i);
      if (limit < d) {
        break;
      }
      if (d.getDay() === 1) { // monday
        weekIdxOfYear++;
      }
      __woyMap[fmtDate2(d)] = weekIdxOfYear;
    }
  });
}

function weekOfYear(date) {
  return __woyMap[date];
}

// --------------------------------

function dateCell(date) {
  const woy = weekOfYear(date);
  let dateClass = "";
  puts(79, woy);
  if (woy % 2 === 0) {
    dateClass = "woy_even";
  }

  return TreeBuilder.build(h =>
    h("th", {
        title: date
      , "class": dateClass
      }
    , fmtDate(date)
    )
  );
}

class LogTaskHeadRow {
  static render(state, dates) {
    initWoyMap(dates);

    return TreeBuilder.build(h => [
      h("tr", {}
      , h("th", {}, "id")
      , h("th", {}, "name")
      , h("th", {}, "worker")
      , dates.map(dateCell)
      )
    , h("tr", {}
      , h("th", {}, "")
      , h("th", {}, "")
      , h("th", {}, "")
      , dates.map(date => h("th", {}
        , h("a"
          , { href: `/workers/${state.selectedWorker}/logs/${date}/edit` }
          , "edit")
          )
        )
      )
    ]);
  }
}

class LogTaskRow {
  static render(task) {
    return TreeBuilder.build(h =>
      h("tr", {}
      , h("th", {}
        , h("a", { href: `/tasks/${task.id}/edit` }, task.id)
        )
      , h("th", {}, task.name)
      , h("th", {}, task.worker)
      , task._logs.map(log =>
            h("td", { "class": "number-cell" }, (log ? log.time : "")
          )
        )
      )
    );
  }
}

class LogSection {
  static render(state) {
    const dates = state.logs.map(log => log.date);

    const workerTasks =
      state.tasks
        .filter(t => t.worker === state.selectedWorker);
    const workerTaskIds =
      workerTasks.map(t => t.id);

    const calcTotalTime = (logs, date) => {
      const log = logs.find(log => log.date === date);
      return __g.sum(
        log.tasks
          .filter(t => workerTaskIds.includes(t.id))
          .map(t => t.time)
      );
    };

    return TreeBuilder.build(h =>
      h("div", {}
      , h("h1", {}, "log")
      , h("div", {}
        , h("button", { onclick: ()=>{ __p.oninput_addBtn(); } }, "追加")
        )
      , h("table", {}
        , LogTaskHeadRow.render(state, dates)
        , workerTasks
          .map(task => LogTaskRow.render(task))
        , h("tr", {}
          , h("th", {}, "-")
          , h("th", {}, "total")
          , h("th", {}, "-")
          , dates.map(date => h("td", { "class": "number-cell"
            , style: { background: "#f4f4f4" }
            }
          , calcTotalTime(state.logs, date)))
          )
        )
      )
    );
  }
}

// --------------------------------

class View {
  static render(state){
    return TreeBuilder.build(h =>
      h("div", {}

/*
      , MyRadioGroup.render(
          "radio_group_workers"
        , state._workers.map(w => ({
            value: w, text: w
          }))
        , {
            selected: state.selectedWorker
          , onchange: (ev)=>{
              __p.onchange_worker(ev);
            }
          }
        )
*/

      , LogSection.render(state)

      , h("textarea"
        , {
            id: "custom_report"
          , style: {
              width: "50%"
            , height: "8rem"
            , fontSize: "0.9rem"
            }
          }
        , __p.getCustomReport()
        )
      )
    );
  }
}

class Page {
  constructor(){
    this.state = {
      tasks: [],
      logs: [],
      selectedWorker: "foo"
    };
  }

  getTitle(){
    return "dashboard";
  }

  preprocState(result) {
    Object.assign(this.state, result);

    // 使いやすさのために変形して持たせておく
    this.state.tasks.forEach(task => {
      // TODO データがない日は持たないようにしたい
      task._logs =
        this.state.logs
          .map(log => {
            const t = log.tasks.find(t => t.id === task.id)
            if (t != null) {
              return {
                time: t.time,
                date: log.date
              };
            } else {
              return null;
            }
          });
    });

    (()=>{
      const map = {};
      this.state.tasks
        .map(t => t.worker)
        .filter(w => w)
        .forEach(w => map[w] = null);
      this.state._workers = Object.keys(map);
      // this.state.selectedWorker = this.state._workers[0];
    })();

    this.state.selectedWorker = this.getWorker();
  }

  getWorker() {
    const m = location.href.match(/\/workers\/(.+?)\//);
    return m[1];
  }

  init(){
    puts("init");
    __g.api_v2("get", "/api/dashboard", {}, (result)=>{
      __g.unguard();
      puts(result);
      this.preprocState(result);

      this.render();

    }, (errors)=>{
      __g.unguard();
      __g.printApiErrors(errors);
      alert("Check console.");
    });
  }

  render(){
    $("#tree_builder_container")
      .empty()
      .append(View.render(this.state));
  }

  onchange_worker(ev){
    this.state.selectedWorker = MyRadioGroup.getValue(ev);
    puts("woker => " + this.state.selectedWorker);
    this.render();
  }

  oninput_addBtn() {
    const lastDate = this.state.logs.sort().reverse()[0].date;
    const newDate = window.prompt("new date", lastDate);
    if (newDate == null) {
      return;
    }
    if (! /^....-..-..$/.test(newDate)) {
      throw new Error("invalid format");
    }

    __g.api_v2(
      "post",
      `/api/workers/${this.state.selectedWorker}/logs/`, {
        lastDate: lastDate,
        date: newDate
      },
      (result)=>{
        __g.unguard();
        puts(result);

        location.reload();
      },
      (errors)=>{
        __g.unguard();
        __g.printApiErrors(errors);
        alert("Check console.");
      }
    );
  }

  getCustomReport() {
    let s = "";
    this.state.tasks.forEach(t => {
      // puts(245, t);
      s += `- ${t.id} ${t.name}`;
      s += "\n";
      s += `  - ...`;
      s += "\n";
    });
    return s;
  }
}

__g.ready(new Page());
