const D_HS = 6;

function round(x, digit) {
  if (isNaN(x)) {
    return NaN;
  } else if (x == null) {
    return null;
  }

  const p = Math.pow(10, digit);

  return Math.round(x * p) / p;
};

// --------------------------------

class TaskRow {
  static render(task) {
    const fmtFloat = (x, digit) => {
      if (isNaN(x)) {
        return "N/A";
      }

      return round(x, digit);
    };

    const dates = task._logs
          .filter(log => log != null)
          .map(log => log.date)
          .sort();

    const fmtDate = (d) => {
      if (d == null) {
        return "";
      }

      const m = d.match(/^(....)-(.+)/);
      return m[2];
    }

    const dateDiff = (dates) => {
      if (dates.length === 0) {
        return "N/A";
      }

      const d0 = Date.parse(dates[0]);
      const d1 = Date.parse(dates[dates.length - 1]);
      return Math.round((d1 - d0) / (60 * 60 * 24 * 1000)) + 1;
    }

    const logs = task._logs
      .filter(log => log != null);

    const na = () => TreeBuilder.build(h => h("span", { "class": "na" }, "N/A"));

    let actual;
    if (task.actual) {
      // 日々のログがなく、actual が明示的に記入されている場合はそちらを使う
      actual = task.actual;
    } else {
      actual = __g.sum(logs.map(log => log.time));
    }

    let ratioEstimated = na();
    if (task.state === "done") {
      ratioEstimated = fmtFloat(actual / task.estimated, 2);
    }

    let ratioPredicted = na();
    if (task.state === "done") {
      ratioPredicted = fmtFloat(actual / task.predicted, 2);
    }

    return TreeBuilder.build(h =>
      h("tr", {}
      , h("th", {}, task.id)
      , h("th", {}, task.name)
      , h("td", {}
        , h("a", {
              href: `/workers/${task.worker}/logs`
            }
          , task.worker
          )
        )
      , h("td", {}, task.state)
      , h("td", {}, fmtDate(dates[0]))
      , h("td", {}, fmtDate(dates[dates.length - 1]))
      , h("td", {}, dateDiff(dates))
      , h("td", { "class": "number-cell" }, task.estimated)
      , h("td", { "class": "number-cell" }, task.predicted)

      , h("td", { "class": "number-cell" }, actual)
      , h("td", { "class": "number-cell" }, fmtFloat(actual / D_HS, 1))

      , h("td", { "class": "number-cell" }, ratioEstimated)
      , h("td", { "class": "number-cell" }, ratioPredicted)
      )
    );
  }
}

class TaskSection {
  static render(state) {
    const fmtDate = (date) => {
      // return date.replaceAll("-", "\n-");
      return date;
    }

    const dates = state.logs.map(log => log.date);

    const tasks =
      state.tasks.map(task => {
        const logs =
          state.logs.map(log => {
            return log.tasks.find(_task => _task.id === task.id);
          });
        task.logs = logs;
        return task;
      });

    const calcTotalTime = (logs, date) => {
      const log = logs.find(log => log.date === date);
      return __g.sum(
        log.tasks.map(t => t.time)
      );
    };
    
    return TreeBuilder.build(h =>
      h("div", {}
      , h("h1", {}, "task")
      , h("table", {}
        , h("tr", {}
          , h("th", {}, "id")
          , h("th", {}, "name")
          , h("th", {}, "worker")
          , h("th", {}, "state")
          , h("th", {}, "start")
          , h("th", {}, "end")
          , h("th", {}, "days")
          , h("th", {}, "雑見積 (h)")
          , h("th", {}, "機械的な予測 (h)")
          , h("th", {}, "現実 (h)")
          , h("th", {}, "現実 (d)")
          , h("th", {}, "雑見積の何倍かかったか")
          , h("th", {}, "機械的な予測の何倍かかったか")
          )
        , state.tasks.map(task => TaskRow.render(task))
        )
      )
    );
  }
}

class View {
  static render(state){
    return TreeBuilder.build(h =>
      h("div", {}

/*
      , MyRadioGroup.render(
          "radio_group_workers"
        , state._workers.map(w => ({
            value: w, text: w
          }))
        , {
            selected: state.selectedWorker
          , onchange: (ev)=>{
              __p.onchange_worker(ev);
            }
          }
        )
*/

      , TaskSection.render(state)
      )
    );
  }
}

class Page {
  constructor(){
    this.state = {
      tasks: [],
      logs: [],
      selectedWorker: "foo"
    };
  }

  getTitle(){
    return "dashboard";
  }

  preprocState(result) {
    Object.assign(this.state, result);

    // 使いやすさのために変形して持たせておく
    this.state.tasks.forEach(task => {
      // TODO データがない日は持たないようにしたい
      task._logs =
        this.state.logs
          .map(log => {
            const t = log.tasks.find(t => t.id === task.id)
            if (t != null) {
              return {
                time: t.time,
                date: log.date
              };
            } else {
              return null;
            }
          });
    });

    (()=>{
      const map = {};
      this.state.tasks
        .map(t => t.worker)
        .filter(w => w)
        .forEach(w => map[w] = null);
      this.state._workers = Object.keys(map);
      this.state.selectedWorker = this.state._workers[0];
    })();
  }

  init(){
    puts("init");
    __g.api_v2("get", "/api/dashboard", {}, (result)=>{
      __g.unguard();
      puts(result);
      this.preprocState(result);

      this.render();

    }, (errors)=>{
      __g.unguard();
      __g.printApiErrors(errors);
      alert("Check console.");
    });
  }

  render(){
    $("#tree_builder_container")
      .empty()
      .append(View.render(this.state));
  }

  onchange_worker(ev){
    this.state.selectedWorker = MyRadioGroup.getValue(ev);
    puts("woker => " + this.state.selectedWorker);
    this.render();
  }

  oninput_addBtn() {
    const lastDate = this.state.logs.sort().reverse()[0].date;
    const newDate = window.prompt("new date", lastDate);
    if (newDate == null) {
      return;
    }
    if (! /^....-..-..$/.test(newDate)) {
      throw new Error("invalid format");
    }

    __g.api_v2(
      "post",
      `/api/workers/${this.state.selectedWorker}/logs/`, {
        lastDate: lastDate,
        date: newDate
      },
      (result)=>{
        __g.unguard();
        puts(result);

        location.reload();
      },
      (errors)=>{
        __g.unguard();
        __g.printApiErrors(errors);
        alert("Check console.");
      }
    );
  }

}

__g.ready(new Page());
