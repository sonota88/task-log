class View {
  static render(state) {
    return TreeBuilder.build(h =>
      h("div", {
        }
      , h("textarea", {
            id: "task_main"
          , style: {
              height: "12rem", width: "90%"
            }
          }
        , JSON.stringify(state.task, null, "  ")
        )
      , h("br")
      , h("textarea", {
            id: "task_memo"
          , style: {
              height: "12rem", width: "90%"
            }
          }
        , state.task.memo
        )
      , h("br")
      , h("button", {
            onclick: ()=>{ __p.onclick_save(); }
          }
        , "更新"
        )
      )
    );
  }
}

class Page {
  constructor(){
    this.state = {
    };
  }

  getTitle(){
    return "タスク編集";
  }

  preprocState(result) {
    Object.assign(this.state, result);
  }

  getTaskId() {
    const m = location.href.match(/\/tasks\/(.+?)\/edit/);
    return m[1];
  }

  init() {
    const taskId = this.getTaskId();

    puts("init");
    __g.api_v2("get", `/api/tasks/${taskId}`, {}, (result)=>{
      __g.unguard();
      puts(result);
      this.preprocState(result);

      this.render();

    }, (errors)=>{
      __g.unguard();
      __g.printApiErrors(errors);
      alert("Check console.");
    });
  }

  render(){
    $("#tree_builder_container")
      .empty()
      .append(View.render(this.state));
  }

  // events

  onclick_save() {
    const taskId = this.getTaskId();

    const task = JSON.parse($("#task_main").val());
    task.memo = $("#task_memo").val() || "";

    __g.api_v2("put", `/api/tasks/${taskId}`,
      {
        task: task
      },
      (result)=>{
        location.reload();
      }, (errors)=>{
        __g.unguard();
        __g.printApiErrors(errors);
        alert("Check console.");
      }
    );
  }

}

__g.ready(new Page());
