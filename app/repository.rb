class Repository
  def json_load(path)
    JSON.parse(File.read(path))
  end

  def json_save(path, data)
    File.open(path, "wb") { |f| f.print JSON.pretty_generate(data) }
  end

  def file_path_including_date(date)
    year = date[0..3]

    data_dir = File.expand_path("../data")
    dir = File.join(data_dir, "task_log")

    path =
      Dir.glob("#{dir}/#{year}_*.json").find { |_path|
        data = json_load(_path)
        data["logs"].any? { |log| log["date"] == date }
      }

    path
  end

  def file_path_including_task(task_id)
    data_dir = File.expand_path("../data")
    dir = File.join(data_dir, "task_log")

    # 新しい方から
    Dir.glob("#{dir}/*.json").sort.reverse_each { |_path|
      data = json_load(_path)
      if data["tasks"].any? { |t| t["id"] == task_id }
        return _path
      end
    }
  end

  def get_all
    data_dir = File.expand_path("../data")
    path = File.join(data_dir, "task_log/2021_01.json")
    data = json_load(path)

    path2 = File.join(data_dir, "task_log/2022_01.json")
    data2 = json_load(path2)

    tasks = []
    # 後勝ち
    data2["tasks"].each { |task|
      unless tasks.any? { |t| t["id"] === task["id"] }
        tasks << task
      end
    }
    data["tasks"].each { |task|
      unless tasks.any? { |t| t["id"] === task["id"] }
        tasks << task
      end
    }

    logs = []
    logs += data["logs"]
    logs += data2["logs"]

    _data = {
      "tasks" => tasks,
      "logs" => logs
    }

    _data
  end

  def create_log_task(last_date, date)
    path = file_path_including_date(last_date)

    data = json_load(path)

    new_lt = {
      date: date,
      tasks: [],
      memo: ""
    }

    data["logs"] << new_lt

    json_save(path, data)
  end

  def update_log_tasks(log, worker:, date:)
    path = file_path_including_date(date)
    data = json_load(path)

    target_task_ids =
      data["tasks"]
        .select { |t| t["worker"] == worker }
        .map { |t| t["id"] }

    log_stored =
      data["logs"].find { |_log|
        _log["date"] == date
      }

    lts_other = []
    log_stored["tasks"].each { |lt|
      unless target_task_ids.include?(lt["id"])
        # 対象害のものを退避
        lts_other << lt
      end
    }

    log_stored["tasks"] = (lts_other + log[:tasks]).sort_by { |it| it["it"] }
    log_stored["memo"] = log[:memo]

    json_save(path, data)
  end

  def update_task(task)
    tid = task[:id]
    path = file_path_including_task(tid)

    data = json_load(path)
    data["tasks"].each { |_task|
      if _task["id"] == tid
        _task["memo"] = task[:memo]
        _task["name"] = task[:name]
      end
    }

    json_save(path, data)
  end
end
